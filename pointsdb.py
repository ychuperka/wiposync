import sqlite3
import time
import math


class PointsDatabase:
    def __init__(self, db_file_path):
        self.__conn = sqlite3.connect(db_file_path)
        self.__cursor = self.__conn.cursor()

        def dict_row_factory(cursor, row):
            """
            Custom row factory form sqlite connection
            :param cursor:
            :type cursor: sqlite3.Cursor
            :param row:
            :return:
            """
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d

        self.__cursor.row_factory = dict_row_factory

    @property
    def cursor(self):
        return self.__cursor

    @property
    def connection(self):
        return self.__conn

    def _get_records_count(self, table_name):
        self.cursor.execute('SELECT COUNT(*) AS `count` FROM `%s`' % table_name)
        return self.cursor.fetchone()['count']

    @staticmethod
    def _calculate_bound(total, page_size):
        return total if total <= page_size else total - page_size

    def _build_results_generator(self, query_sql, query_params, page_size=100):
        current = 0
        while True:
            for row in self.cursor.execute(query_sql + (' LIMIT %d, %d' % (current, page_size)), query_params):
                yield row
            if self.cursor.rowcount < 1:
                break
            current += page_size


class SourceDatabase(PointsDatabase):
    """
    This class represents source database
    """
    def __init__(self, db_file_path):
        PointsDatabase.__init__(self, db_file_path)
        self.__PAGE_SIZE = 100
        self.__bound = self._calculate_bound(self._get_records_count('record'), self.__PAGE_SIZE)

    def urls(self):
        """
        Creates a generator which returns urls to points data
        :return: GeneratorType
        """
        current = 0
        while current < self.__bound:
            for row in self.cursor.execute(
                            'SELECT `url` FROM `record` LIMIT %d, %d' % (current, self.__PAGE_SIZE)):
                yield row['url']
            current += self.__PAGE_SIZE


class DestinationDatabase(PointsDatabase):
    """
    This class represents destination database
    """
    def __init__(self, db_file_path):
        PointsDatabase.__init__(self, db_file_path)
        self.__cluster_id = 0
        self.__POINTS_PAGE_SIZE = 300
        self.__PT_COLUMNS_LIST = '`Z_PK` AS `id`, `ZTITLE` AS `name`, `ZCOMMENT` AS `password`, `ZLAT` AS `lat`, ' \
                                 '`ZLON` AS `lng`'

    @property
    def points_count(self):
        return self._get_records_count('ZPOINT')

    def is_point_with_this_name_exists(self, name):
        """
        Checks that point with specified name already exists
        :param name:
        :return:
        """
        self.cursor.execute('SELECT `Z_PK` FROM `ZPOINT` WHERE `ZTITLE` = ?', (name,))
        return self.cursor.rowcount > 0

    def get_cluster_id(self):
        """
        Tries to obtain "root" cluster id and creates its record if it not found
        :return: int
        """
        if self.__cluster_id:
            return self.__cluster_id

        self.cursor.execute('SELECT `Z_PK` AS `id` FROM `ZCLUSTER` WHERE `ZLAT` = 0 AND `ZLON` = 0')
        if self.cursor.rowcount == 0:
            timestamp = int(time.time())
            self.cursor.execute(
                'INSERT INTO `ZCLUSTER` ' +
                '(`Z_ENT`, `Z_OPT`, `ZIDENTIFIER`, `ZCREATEDATE`, `ZUPDATEDATE`, `ZLAT`, `ZLON`, `ZNUMBEROFPOINTS`) ' +
                'VALUES (0, 0, '', ?, ?, 0, 0, 0)',
                (timestamp, timestamp)
            )
            self.__cluster_id = self.cursor.lastrowid
            self.connection.commit()
        else:
            row = self.cursor.fetchone()
            self.__cluster_id = int(row['id'])

        return self.__cluster_id

    def __increment_cluster_points_count(self, cluster_id):
        self.cursor.execute(
            'UPDATE `ZCLUSTER` SET `ZNUMBEROFPOINTS` = `ZNUMBEROFPOINTS` + 1 WHERE `Z_PK` = ?',
            (cluster_id,)
        )
        self.connection.commit()

    def update_point_password(self, point_id, new_password):
        self.cursor.execute(
            'UPDATE `ZPOINT` SET `ZCOMMENT` = ? WHERE `Z_PK` = ?',
            (point_id, new_password,)
        )
        self.connection.commit()

    def get_points_with_same_position(self, lat, lng):
        """
        Get points with same position
        :param lat:
        :type lat: float
        :param lng:
        :type lat: float
        :return: Generator Type
        """
        return self._build_results_generator(
            'SELECT %s FROM `ZPOINT` WHERE `ZLAT` = ? AND `ZLON` = ?' % self.__PT_COLUMNS_LIST,
            (lat, lng),
            self.__POINTS_PAGE_SIZE
        )

    def get_points_within_a_radius(self, radius, lat, lng):
        """
        Get points within a radius
        :param radius: In meters
        :type radius: int
        :param lat:
        :type lat: float
        :param lng:
        :type lng: float
        :return: GeneratorType
        """
        # Compute max and min lat and lng
        rads_quot = radius / 1000 / 6371  # 6371 - Earth radius
        rads_quot_in_degrees = math.degrees(rads_quot)
        max_lat = lat + rads_quot_in_degrees
        min_lat = lat - rads_quot_in_degrees

        a = math.degrees(math.asin(rads_quot) / math.cos(math.radians(lat)))
        max_lng = lng + a
        min_lng = lng - a

        # Select points within a radius
        return self._build_results_generator(
            'SELECT %s FROM `ZPOINT` WHERE `ZLAT` <= ? AND `ZLAT` >= ? AND `ZLON` <= ? AND `ZLON` >= ?' %
            self.__PT_COLUMNS_LIST,
            (max_lat, min_lat, max_lng, min_lng),
            self.__POINTS_PAGE_SIZE
        )

    def create_point_record(self, name, lat, lng, address, identifier, cluster_id, created_at, password):
        """
        Creates a point record
        :param name:
        :param lat:
        :param lng:
        :param address:
        :param identifier:
        :param cluster_id:
        :param created_at:
        :param password:
        :return:
        """
        self.cursor.execute(
            'INSERT INTO `ZPOINT` ' +
            '(`ZTITLE`, `ZLAT`, `ZLON`, `ZADDRESS`, `ZIDENTIFIER`, `ZCLUSTER`, `ZCREATEDATE`, `ZUPDATEDATE`, ' +
            ' `ZCOMMENT`, `ZISOPEN`, `Z_ENT`, `Z_OPT`) ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 2, 1)',
            (
                name, lat, lng, address, identifier, cluster_id, created_at, int(time.time()), password,
                not bool(password)
            )
        )
        rowid = self.cursor.lastrowid
        self.connection.commit()
        self.__increment_cluster_points_count(cluster_id)
        return rowid
