import hashlib

from worker import Worker


class Consumer(Worker):

    def __init__(self, destination_db, message_queue=None):
        """
        :param destination_db: Destination database
        :type destination_db: pointsdb.DestinationDatabase
        :param message_queue: A Message queue
        :type message_queue: queue.Queue
        """
        Worker.__init__(self, destination_db, message_queue)
        self.__MAX_POINTS_DISTANCE = 50

    @property
    def points_count(self):
        return self.db.points_count

    def save_point(self, point_data):
        """
        Saves the point data
        :param point_data:
        :return:
        """
        self.__add_point_info_to_messages(point_data)
        point_name = self.get_point_name(point_data)
        if not point_name:
            self._add_message('Hmm.. It seems this point has empty name, let\'s skip it.')
            return False

        cluster_id = self.db.get_cluster_id()
        lat = float(point_data['lat'])
        lng = float(point_data['lng'])
        # Check each point with same coordinates
        for same_point in self.db.get_points_with_same_position(lat, lng):
            if same_point['name'] == point_name:
                self._add_message('Point already exists.')
                return True

        # Try to find point within a radius
        for same_point in self.db.get_points_within_a_radius(self.__MAX_POINTS_DISTANCE, lat, lng):
            if same_point['name'] == point_name:
                self._add_message('Point already exists, it position within a %d meters radius.' %
                                  self.__MAX_POINTS_DISTANCE)
                return True

        # No points found, create a new one
        rowid = self.__create_point(cluster_id, point_data)
        if rowid > 0:
            self._add_message('A new point saved, name: %s, id: %d' % (point_name, rowid))
            return True
        else:
            return False

    def __create_point(self, cluster_id, point_data):
        """
        Create new point
        :param cluster_id:
        :param point_data:
        :return:
        """
        point_name = self.get_point_name(point_data)
        identifier = hashlib.sha1((point_name + point_data['lat'] + point_data['lng']).encode('utf-8')).hexdigest()
        return self.db.create_point_record(
                self.get_point_name(point_data), point_data['lat'], point_data['lng'],
                point_data['address'] if 'address' in point_data else '',
                identifier, cluster_id, self.get_point_timestamp(point_data),
                self.get_point_password(point_data)
            )

    def __add_point_info_to_messages(self, point_data):
        self._add_message(
            'Processing point "%s", lat: %s, lng: %s' %
            (self.get_point_name(point_data), point_data['lat'], point_data['lng'])
        )

    @staticmethod
    def get_point_name(point_data):
        if 'name' in point_data and point_data['name']:
            return point_data['name']
        elif 'ssid' in point_data and point_data['ssid']:
            return point_data['ssid']
        else:
            return None

    @staticmethod
    def get_point_timestamp(point_data):
        if 'tips' in point_data and point_data['tips']:
            return point_data['tips'][0]['createdAt']
        else:
            return None

    @staticmethod
    def get_point_password(point_data):
        password = None
        last_created_at = 0
        if 'tips' in point_data and point_data['tips']:
            for tip in point_data['tips']:
                if 'password' in tip and 'createdAt' in tip and tip['createdAt'] > last_created_at:
                    last_created_at = tip['createdAt']
                    password = tip['password']
        return password
