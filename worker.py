class Worker:

    def __init__(self, db, message_queue=None):
        """
        :param db:
        :type db: pointsdb.PointsDatabase
        :param message_queue:
        :type message_queue: queue.Queue
        """
        self.__db = db
        self.__message_queue = message_queue

    @property
    def db(self):
        return self.__db

    def _add_message(self, message):
        """
        Add a new message to a queue
        :param message:
        :type message: string
        :return: bool
        """
        if not self.__message_queue:
            return False
        self.__message_queue.put(message)
        return True
