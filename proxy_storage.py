import sqlite3
import time


class ProxyStorage:
    def __init__(self, interval):
        """
        :param interval: Interval in which a proxy will be used again (seconds)
        :type interval: int
        """
        self.__conn = sqlite3.connect(':memory:')
        self.__init_database(self.__conn)
        self.__interval = interval

    @staticmethod
    def __init_database(conn):
        """
        Init a database
        :param conn:
        :type conn: sqlite3.Connection
        :return:
        """
        sql = '''
        CREATE TABLE `p` (
            `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            `host` TEXT UNIQUE NOT NULL,
            `port` INTEGER NOT NULL,
            `alive` INTEGER DEFAULT '1',
            `locked` INTEGER DEFAULT '0',
            `last_use_timestamp` INTEGER DEFAULT NULL
        );
        '''
        conn.execute(sql)

    def add(self, host, port):
        """
        Add a new proxy
        :param host:
        :param port:
        :return:
        """
        cursor = self.__conn.cursor()
        cursor.execute('INSERT INTO `p` (`host`, `port`, `last_use_timestamp`) VALUES (?, ?, NOW())', (host, port))
        self.__conn.commit()
        return cursor.lastrowid

    def get_next(self):
        """
        Get next proxy
        :return: Will return a tuple (id, host, port)
        """
        # Get a proxy
        bound = int(time.time()) - self.__interval
        cursor = self.__conn.cursor()
        cursor.execute(
            'SELECT `id`, `host`, `port` FROM `p` '
            'WHERE `locked` = 0 AND `alive` = 1 AND `last_use_timestamp` < ? LIMIT 1',
            (bound,)
        )

        if cursor.rowcount < 0:
            raise ProxyStorageEmptyError()

        # Mark selected proxy as locked
        row = cursor.fetchone()
        cursor.execute('UPDATE `p` SET `locked` = 1 WHERE `id` = ?', (row[0],))
        self.__conn.commit()

        return row

    def mark_as_dead(self, id):
        cursor = self.__conn.cursor()
        cursor.execute('UPDATE `p` SET `alive` = 0 WHERE `id` = ?', (id,))
        self.__conn.commit()


class ProxyStorageEmptyError(Exception):
    pass
