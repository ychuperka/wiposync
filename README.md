Installation
============

Python interpreter
------------------
Download an installer from "https://www.python.org/downloads/", you need a version >=3.5

Dependencies
-----------
> pip3 install requirements.txt

Usage
=====
- Open a terminal emulator (you can use iTerm on OSX).
- Go to the directory where you unpacked the software
- Install dependencies
- Run a script using following command: > python3 main.py source_database_file_path destination_database_file_path [-v|--verbose]
- If no -v or --verbose option provided then output will not be sent to stdout
- Type > python3 main.py -h or --help to see a help message