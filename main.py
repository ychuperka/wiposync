import argparse
import time
import sys

from threading import Thread
from queue import Queue
from consumer import Consumer
from pointsdb import SourceDatabase, DestinationDatabase
from producer import Producer


def producer_process(source_db_path, message_queue, points_queue):
    """
    Producer process body
    :param source_db_path:
    :type source_db_path: string
    :param message_queue:
    :type message_queue: Queue
    :param points_queue:
    :type points_queue: Queue
    :return:
    """
    producer = Producer(SourceDatabase(source_db_path), message_queue)
    for item in producer.points():
        if not item['status']:
            continue
        points_queue.put(item['point'])
    points_queue.put('done')


def consumer_process(destination_db_path, message_queue, points_queue):
    """
    Consumer process body
    :param destination_db_path:
    :type destination_db_path: string
    :param message_queue:
    :type message_queue: Queue
    :param points_queue:
    :type points_queue: Queue
    :return:
    """
    consumer = Consumer(DestinationDatabase(destination_db_path), message_queue)
    initial_points_count = consumer.points_count

    while True:
        item = points_queue.get()
        if item == 'done':
            points_queue.task_done()
            break
        consumer.save_point(item)
        points_queue.task_done()

    final_points_count = consumer.points_count
    message_queue.put('%d points was added.' % (final_points_count - initial_points_count))


def run():

    parser = argparse.ArgumentParser(description='wifimap.io to wifimaps.net sync tool')
    parser.add_argument('source_db', type=str, help='Source database file path')
    parser.add_argument('destination_db', type=str, help='Destination database file path')
    parser.add_argument('-v', '--verbose', dest='verbose', help='Verbose mode', action='store_true')
    args = parser.parse_args()

    message_queue = Queue()
    points_queue = Queue(maxsize=4096)

    producer = Thread(target=producer_process, args=(args.source_db, message_queue, points_queue))
    producer.start()

    consumer = Thread(target=consumer_process, args=(args.destination_db, message_queue, points_queue))
    consumer.start()

    def flush_message_queue(queue):
        while not queue.empty():
            print(queue.get())

    while consumer.is_alive():
        if args.verbose:
            flush_message_queue(message_queue)
        time.sleep(0.2)
    flush_message_queue(message_queue)

    points_queue.join()
    producer.join()
    consumer.join()

    return 0

if __name__ == '__main__':
    sys.exit(run())
