import requests
import time

from worker import Worker
from json.decoder import JSONDecodeError


class Producer(Worker):
    """
    This class represents a producer, it produces points data
    """
    def __init__(self, source_db, message_queue=None):
        Worker.__init__(self, source_db, message_queue)
        self.__RETRIES_COUNT = 5
        self.__RETRY_WAIT_SEC = 5

    def points(self):
        """
        This method creates a generator which returns points data.
        Points data is wrapper into a dictionary like this:
        {
            'status': True|False, - will be False if something went wrong
            'point': {}, - point data
            'message': '...' - here will be a message if something went wrong
        }
        :return: GeneratorType
        """
        for url in self.db.urls():

            self._add_message('Requesting point data from: %s' % url)

            r = requests.get(url)
            if not r.ok:
                self._add_message('Can`t request data, http response code: %d, url: %s' % (r.status_code, url))
                yield self._wrap_item(False)

            items = None
            for i in range(0, self.__RETRIES_COUNT):
                try:
                    items = r.json()
                    break
                except JSONDecodeError as e:
                    self._add_message('ERROR! Can\'t parse server response(json), reason: %s' % e.msg)
                    self._add_message('Try number is: %d. Will try again in %d sec.' % (i, self.__RETRY_WAIT_SEC))
                    time.sleep(self.__RETRY_WAIT_SEC)
                    continue

            if not items:
                continue

            for i in items['add']:
                yield self._wrap_item(True, i)

    @staticmethod
    def _wrap_item(status, item=None):
        """
        Just wraps point data into a dictionary
        :param status: True or False
        :param item: Point data
        :return: DictType
        """
        return {
            'status': status,
            'point': item,
        }
